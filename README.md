This is a quick Proof of concept showing one way to solve a question that was asked on the CIALUG mailing list.

It assumes that you already have .net 5 installed on your machine. 

Run with the command ```dotnet run```
