using System.Collections.Generic;

namespace path
{
    public class Node
    {
        public Node(int name)
        {
            Name= name;
            NextNeighbor=new List<Node>();
        }
        public void Add(Node n)
        {
            NextNeighbor.Add(n);
        }
        public List<Node> NextNeighbor {get;set;}
        public int Name {get;set;}     
    }
}