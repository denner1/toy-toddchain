﻿using System;

namespace path
{
    class Program
    {
        static void Main(string[] args)
        {
            Graph g = new Graph();
            g.Add(1,2);
            g.Add(2,3);
            g.Add(2,4);
            g.Add(3,6);
            foreach( var r in g.TraverseSet())
                Console.WriteLine(string.Join(",",r));
        }
    }
}
