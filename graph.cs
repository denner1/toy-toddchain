using System.Collections.Generic;
using System.Linq;
namespace path
{
    public class Graph
    {
        private Dictionary<int, Node> nodes = new Dictionary<int, Node>();
        public void Add(int i, int j)
        {
            if (!nodes.ContainsKey(i))
                nodes[i]=new Node(i);
            if (!nodes.ContainsKey(j))
                nodes[j]= new Node(j);
            nodes[i].Add(nodes[j]);
        }
        public List<List<int>> TraverseSet()
        {
            List<List<int>> collector=new List<List<int>>();
            foreach(var n in nodes)
                collector.AddRange(this.TraverseNode(n.Value).Where(x=>x.Count()>1)); //Wall across all nodes and eliminate results from orphaned nodes
            return collector;
        }
        private List<List<int>> TraverseNode(Node n)
        {
            //recursively build up the set of results
            //
            // For all neighbors of node, build up the list of paths, and then prepend our name to it
            var collector = n.NextNeighbor.SelectMany(x=>{
                List<List<int>> paths=new List<List<int>>();
                paths.AddRange( TraverseNode(x).Select(y=>
                {
                    List<int> path = new List<int>(){n.Name};
                    path.AddRange(y);
                    return path;
                }).ToList());
                return paths;
            }).ToList();
            if (!collector.Any()) //this is the base case, just return a list of one, ourselves
                return new List<List<int>>(){new List<int>(){n.Name}};
            return collector; //else return the list of paths we just built
        }
    }    
}